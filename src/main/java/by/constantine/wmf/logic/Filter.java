package by.constantine.wmf.logic;

import by.constantine.wmf.utils.Converter;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Filter {

    public static BufferedImage filter(BufferedImage image, javax.swing.JTable matrix) {
        if (image == null) {
            return null;
        }
        int size = matrix.getColumnCount();
        Integer[][] mask = new Integer[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                try {
                    mask[i][j] = (Integer) matrix.getValueAt(i, j);
                } catch (Exception e) {
                    mask[i][j] = 0;
                }
            }
        }
        return filter(image, mask);
    }

    public static BufferedImage filter(BufferedImage image, Integer[][] mask) {
        int[][] pixels = Converter.getArrayByImage(image);
        int width = pixels.length,
                height = pixels[0].length,
                maskLength = mask.length,
                xStart, yStart = xStart = maskLength / 2;
        int[][] result = new int[width][height];
        for (int x = xStart; x < width - xStart; x++) {
            for (int y = yStart; y < height - yStart; y++) {
                List<Integer> reds = new ArrayList<>(),
                        greens = new ArrayList<>(),
                        blues = new ArrayList<>(),
                        alphas = new ArrayList<>();
                for (int w = 0; w < maskLength; w++) {
                    for (int h = 0; h < maskLength; h++) {
                        for (int i = 0; i < mask[w][h]; i++) {
                            int pixel = pixels[x + w - xStart][y + h - yStart];
                            int alpha = (pixel >> 24) & 0x000000FF;
                            alphas.add(alpha);
                            int red = (pixel >> 16) & 0x000000FF;
                            reds.add(red);
                            int green = (pixel >> 8) & 0x000000FF;
                            greens.add(green);
                            int blue = pixel & 0x000000FF;
                            blues.add(blue);
                        }
                    }
                }
                Collections.sort(reds);
                Collections.sort(greens);
                Collections.sort(blues);
                Collections.sort(alphas);
                int red = reds.get(reds.size() / 2);
                int green = greens.get(greens.size() / 2);
                int blue = blues.get(blues.size() / 2);
                int alpha = alphas.get(alphas.size() / 2);
                result[x][y] = (alpha << 24) | (red << 16) | (green << 8) | blue;
            }
        }
        return Converter.getImageByArray(result, image.getType());
    }
}
