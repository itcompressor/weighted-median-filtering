/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package by.constantine.wmf.utils;

import java.awt.Graphics;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

/**
 *
 * @author constantine
 */
public class Painter {

    private Integer maxWidth, maxHeight;

    private Graphics g;
    private BufferedImage original;
    private BufferedImage image;

    public Painter(Integer maxWidth, Integer maxHeight) {
        this.maxWidth = maxWidth;
        this.maxHeight = maxHeight;
    }

    public Painter() {
    }

    public BufferedImage getOriginal() {
        return original;
    }

    public void setOriginal(BufferedImage original) {
        this.original = original;
    }

    public Graphics getG() {
        return g;
    }

    public void setG(Graphics g) {
        this.g = g;
    }

    public BufferedImage getImage() {
        return image;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }

    public void paint() {
        if (null != g) {
            g.clearRect(0, 0, 2000, 2000);
            if (maxWidth != null) {
                image = optimumResize(image, maxWidth, maxHeight);
            }
            g.drawImage(image, 0, 0, null);
        }
    }

    private BufferedImage optimumResize(BufferedImage image, int maxWidth, int maxHeight) {
        if(image == null) {
            return null;
        } 
        BufferedImage currentImage = image;
        BufferedImage filterImage = null;
        int differenceWidth = maxWidth - currentImage.getWidth();
        int differenceHeight = maxHeight - currentImage.getHeight();
        while (differenceWidth < 0 || differenceHeight < 0) {
            double maxSide;
            int interimSide;
            if (differenceWidth < differenceHeight) {
                maxSide = maxWidth;
                interimSide = currentImage.getWidth();
            } else {
                maxSide = maxHeight;
                interimSide = currentImage.getHeight();
            }
            if (interimSide > 0 && interimSide > maxSide) {
                double transform = 1.0 / (interimSide / maxSide);
                AffineTransform scale = new AffineTransform();
                scale.scale(transform, transform);
                AffineTransformOp op = new AffineTransformOp(scale, AffineTransformOp.TYPE_BILINEAR);
                Double filterImageWidth = new Double(currentImage.getWidth() * transform);
                Double filterImageHeight = new Double(currentImage.getHeight() * transform);
                filterImage = new BufferedImage(filterImageWidth.intValue(), filterImageHeight.intValue(), currentImage.getType());
                op.filter(currentImage, filterImage);
            }
            differenceWidth = maxWidth - filterImage.getWidth();
            differenceHeight = maxHeight - filterImage.getHeight();
        }
        if (filterImage == null) {
            return currentImage;
        } else {
            return filterImage;
        }
    }
}
