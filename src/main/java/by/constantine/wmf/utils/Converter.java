package by.constantine.wmf.utils;

import java.awt.image.BufferedImage;

public class Converter {

    public static int[][] getArrayByImage(BufferedImage image) {
        int width = image.getWidth();
        int height = image.getHeight();
        int[][] result = new int[height][width];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                try {
                    result[i][j] = image.getRGB(j, i);
                } catch (Exception ex) {
                    System.out.println("Error to array");
                    break;
                }
            }
        }
        return result;
    }

    public static BufferedImage getImageByArray(int[][] pixels, int type) {
        int width = pixels[0].length;
        int height = pixels.length;
        BufferedImage image = new BufferedImage(width, height, type);
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                try {
                    image.setRGB(j, i, pixels[i][j]);
                } catch (Exception ex) {
                    System.out.println("Error to image");
                    break;
                }
            }
        }
        return image;
    }
}
