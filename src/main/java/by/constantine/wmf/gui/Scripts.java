/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package by.constantine.wmf.gui;

/**
 *
 * @author qwer
 */
public class Scripts {

    public static void selectMatrix(javax.swing.JTable matrix, int i) {
        switch (i) {
            case 1:
                matrix.setModel(new javax.swing.table.DefaultTableModel(
                        new Integer[][]{
                            {1, 1, 1, 1, 1},
                            {1, 1, 1, 1, 1},
                            {1, 1, 1, 1, 1},
                            {1, 1, 1, 1, 1},
                            {1, 1, 1, 1, 1}
                        },
                        new String[]{
                            "", "", "", "", ""
                        }
                ));
                break;
            case 2:
                matrix.setModel(new javax.swing.table.DefaultTableModel(
                        new Integer[][]{
                            {1, 1, 1, 1, 1, 1, 1, 1, 1},
                            {1, 1, 1, 1, 1, 1, 1, 1, 1},
                            {1, 1, 1, 1, 1, 1, 1, 1, 1},
                            {1, 1, 1, 1, 1, 1, 1, 1, 1},
                            {1, 1, 1, 1, 1, 1, 1, 1, 1},
                            {1, 1, 1, 1, 1, 1, 1, 1, 1},
                            {1, 1, 1, 1, 1, 1, 1, 1, 1},
                            {1, 1, 1, 1, 1, 1, 1, 1, 1},
                            {1, 1, 1, 1, 1, 1, 1, 1, 1},},
                        new String[]{
                            "", "", "", "", "", "", "", "", ""
                        }
                ));
                break;
            default:
                matrix.setModel(new javax.swing.table.DefaultTableModel(
                        new Integer[][]{
                            {1, 1, 1},
                            {1, 1, 1},
                            {1, 1, 1}
                        },
                        new String[]{
                            "", "", ""
                        }
                ));
                break;
        }
    }

}
